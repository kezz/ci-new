<?php


namespace App\Exception;

use Exception;

final class ShouldNotHappenException extends Exception
{
}
